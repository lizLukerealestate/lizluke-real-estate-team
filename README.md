We believe that buying a home should be fun, and selling a home should be simple. That's why we listen to your needs and lean into two decades of contract and sales experience to lead buyers and sellers like you from frustrated to fulfilled.

Address: 400 King St, Alexandria, VA 22314, USA

Phone: 703-868-5676

Website: https://www.lizluke.com
